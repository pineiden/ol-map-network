import { defaultInit, getStationList } from './index';
const stationId = 'station_list';

const stationList = getStationList(stationId);

defaultInit('map', stationList, 6);
